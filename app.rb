require 'sinatra/reloader' if settings.development?
require './config/boot.rb'

module DomainHelper
  class BaseApp < Sinatra::Base
    configure :development do
      register Sinatra::Reloader
    end

    helpers do
      def render_with_status(status, message, results=nil)
        { code: status, message: message, results: results }.to_json
      end
    end
  end

  class App < BaseApp
    # GET /search
    # params:
    #   domains: an array of domains
    #   query: the query string
    get '/search', provides: :json do
      content_type :json
      if !params[:domains].present? && !params[:query].present?
        render_with_status 400, 'params required'
      elsif !QueryParser.is_valid?(params[:query])
        render_with_status 400, 'invalid query'
      elsif !Lexicon.domain_exists?(params[:domains])
        render_with_status 400, 'invalid domains'
      else
        results = Lexicon.search(params)
        if results.present?
          render_with_status 200, 'success', results
        else
          render_with_status 404, 'not found'
        end
      end
    end
  end
end

