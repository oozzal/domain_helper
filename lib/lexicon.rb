module DomainHelper
  # Lexicon: some cool name for 'Dictionary'
  # Conventions:
  #   list of domains are stored in a set named :domains
  #   list of words for a domain are stored in a sorted set with each word's frequency as its score
  class Lexicon
    MIN_FREQUENCY = 1.0

    class << self
      # match even if one of the domains exist
      def domain_exists?(domain)
        if domain.is_a?(Array)
          domain.detect {|d| LexiconDB.sismember(:domains, d) }.present?
        else
          LexiconDB.sismember(:domains, domain)
        end
      end

      def add_domain(domain)
        LexiconDB.sadd(:domains, domain)
      end

      # Dangerous Methods!
      # def remove_domain(domain)
      #   LexiconDB.srem(:domains, domain)
      # end

      # # boom! use this carefully
      # def destroy_domain(domain)
      #   LexiconDB.multi do
      #     LexiconDB.srem(:domains, domain)
      #     LexiconDB.del(domain)
      #   end
      # end

      def lexeme_exists?(domain, lexeme)
        LexiconDB.zrank(domain, lexeme).present?
      end

      def update_frequency(domain, lexeme, frequency)
        LexiconDB.zadd(domain, frequency, lexeme)
      end

      def get_frequency(domain, lexeme)
        LexiconDB.zscore(domain, lexeme)
      end

      # adding single lexeme in a domain
      def add_lexeme(domain, lexeme, frequency=nil)
        # be sure the domain is added
        add_domain(domain) unless domain_exists?(domain)

        if lexeme_exists?(domain, lexeme)
          return frequency && update_frequency(domain, lexeme, frequency)
        else
          LexiconDB.zadd(domain, frequency || MIN_FREQUENCY, lexeme)
        end
      end

      def search(params)
        matches = []
        params[:domains].each do |domain|
          next unless domain_exists?(domain)
          matches += LexiconDB.zscan_each(
            domain,
            {match: "*#{params[:query]}*"}
          ).to_a
        end

        return matches unless matches.present?

        # matches sample:
        # [['photoshop:Photoshop', 99], ['photostudio:PhotoStudio', 34]]
        matches.sort_by! {|r| -r.last}
        results = matches.map {|match| match.first.split(":").last}
        results.uniq
      end
    end
  end
end

