module DomainHelper
  class QueryParser

    def self.is_valid?(query)
      return false if !query.present? || query.length < 3

      special = "?<>',?[]}{=-)(*&^%$#`~{}"
      regex = /[#{special.gsub(/./){|char| "\\#{char}"}}]/

      !(query =~ regex)
    end

  end
end

