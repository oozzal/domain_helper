require 'rubygems'
require 'bundler'

RACK_ENV = ENV['RACK_ENV'] || 'development'
Bundler.require(:default, RACK_ENV.to_sym)

require 'logger'

APP_ROOT = File.expand_path('..', __FILE__)

require File.join(APP_ROOT, 'app.rb')

logfile = File.join(APP_ROOT, 'log', "#{RACK_ENV}.log")
class ::Logger; alias_method :write, :<<; end

logger  = Logger.new(logfile, 'weekly')
use Rack::CommonLogger, logger

run Rack::URLMap.new('/' => DomainHelper::App)

