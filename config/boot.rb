RACK_ENV ||= ENV['RACK_ENV'] || 'development'

APP_ROOT ||= File.expand_path('../..', __FILE__)

Dir["#{APP_ROOT}/lib/**/*.rb"].each {|f| require f}

REDIS_CONFIG = YAML.load_file(File.join(APP_ROOT, 'config', 'redis.yml'))[RACK_ENV]

LexiconDB = Redis.new(REDIS_CONFIG)

