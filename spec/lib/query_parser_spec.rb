require 'spec_helper'

describe DomainHelper::QueryParser do
  let(:parser) { DomainHelper::QueryParser }

  describe '.is_valid?' do
    context 'when query is absent' do
      let(:query) { '' }

      it 'returns false' do
        expect(parser.is_valid?(query)).to eql(false)
      end
    end

    context 'when query is short' do
      let(:query) { 'ab' }

      it 'returns false' do
        expect(parser.is_valid?(query)).to eql(false)
      end
    end

    context 'when query contains special characters' do
      let(:query) { 'abc$%' }

      it 'returns false' do
        expect(parser.is_valid?(query)).to eql(false)
      end
    end

    context 'when query is valid' do
      let(:query) { 'photo' }

      it 'returns true' do
        expect(parser.is_valid?(query)).to eql(true)
      end
    end
  end
end

