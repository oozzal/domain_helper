require 'spec_helper'

describe DomainHelper::Lexicon do
  let(:lexicon) { DomainHelper::Lexicon }

  # defaults for each test case (don't override these)
  let(:domain) { 'photography' }
  let(:domains) { [domain, 'art'] }
  let(:lexeme) { 'photoshop:Photoshop' }
  let(:frequency) { 7.0 }

  before(:each) do
    LexiconDB.flushdb
    LexiconDB.sadd(:domains, domains)
    LexiconDB.zadd(domain, frequency, lexeme)
  end

  after(:each) do
    LexiconDB.flushdb
  end

  describe '.add_domain' do
    context 'when adding a single domain' do
      context 'when domain exists' do
        it 'does not add the domain again' do
          expect(lexicon.add_domain(domain)).to eql(false)
        end
      end

      context 'when domain does not exist' do
        let(:new_domain) { 'fashion' }

        it 'adds the domain' do
          expect(lexicon.add_domain(new_domain)).to eql(true)
        end
      end
    end

    context 'when adding multiple domains' do
      context 'when all of the domains exist' do
        it 'does not add any of the domains again' do
          expect(lexicon.add_domain(domains)).to eql(0)
        end
      end

      context 'when some of the domains already exist' do
        let(:new_domains) { ['art', 'sports'] }

        it 'only adds non-existing domain(s)' do
          expect(lexicon.add_domain(new_domains)).to eql(1)
        end
      end

      context 'when none of the domains exist' do
        let(:new_domains) { ['fashion', 'sports'] }

        it 'adds all of the domains' do
          expect(lexicon.add_domain(new_domains)).to eql(2)
        end
      end
    end
  end

  describe '.remove_domain' do
    context 'when removing a single domain' do
      context 'when domain does not exist' do
        let(:new_domain) { 'fashion' }

        xit 'does nothing and returns false' do
          expect(lexicon.remove_domain(new_domain)).to eql(false)
        end
      end

      context 'when domain exists' do
        xit 'removes the domain' do
          expect(lexicon.remove_domain(domain)).to eql(true)
        end
      end
    end

    context 'when removing multiple domains' do
      context 'when all of the domains exist' do
        xit 'removes all specified domains' do
          expect(lexicon.remove_domain(domains)).to eql(2)
        end
      end

      context 'when some of the domains exist' do
        let(:new_domains) { ['photography', 'sports'] }

        xit 'removes the existing specified domains' do
          expect(lexicon.remove_domain(new_domains)).to eql(1)
        end
      end

      context 'when none of the domains exist' do
        let(:new_domains) { ['sports', 'fashion'] }

        xit 'removes nothing' do
          expect(lexicon.remove_domain(new_domains)).to eql(0)
        end
      end
    end
  end

  describe '.lexeme_exists?' do
    context 'when lexeme is in the domain' do
      it 'returns true' do
        expect(lexicon.lexeme_exists?(domain, lexeme)).to eql(true)
      end
    end

    context 'when lexeme is not in the domain' do
      let(:new_lexeme) { 'photoalbum:PhotoAlbum' }

      it 'returns false' do
        expect(lexicon.lexeme_exists?(domain, new_lexeme)).to eql(false)
      end
    end
  end

  describe '.get_frequency' do
    context 'when lexeme is in the domain' do
      it 'returns the corresponding frequency' do
        expect(lexicon.get_frequency(domain, lexeme)).to eql(frequency)
      end
    end

    context 'when lexeme is not in the domain' do
      let(:new_lexeme) { 'photoalbum:PhotoAlbum' }

      it 'returns nil' do
        expect(lexicon.get_frequency(domain, new_lexeme)).to be_nil
      end
    end
  end

  describe '.update_frequency' do
    context 'when lexeme is in the domain' do
      it 'updates the frequency and returns false' do
        expect(lexicon.update_frequency(domain, lexeme, frequency)).to eql(false)
      end
    end

    context 'when lexeme is not in the domain' do
      let(:new_lexeme) { 'photoalbum:PhotoAlbum' }

      it 'adds lexeme to the domain and returns true' do
        expect(lexicon.update_frequency(domain, new_lexeme, frequency)).to eql(true)
      end
    end
  end

  describe '.add_lexeme' do
    context 'when domain does not exist' do
      let(:new_domain) { 'history' }

      it 'adds the domain' do
        lexicon.add_lexeme(new_domain, lexeme)
        expect(lexicon.domain_exists?(new_domain)).to eql(true)
      end
    end

    context 'when the lexeme is in the domain' do
      context 'when the frequency is specified' do
        let(:new_frequency) { 10.0 }

        it 'updates the frequency' do
          lexicon.add_lexeme(domain, lexeme, new_frequency)
          expect(lexicon.get_frequency(domain, lexeme)).to eql(new_frequency)
        end
      end

      context 'when the frequency is not specified' do
        it 'does nothing' do
          expect(lexicon.add_lexeme(domain, lexeme)).to be_nil
        end
      end
    end

    context 'when the lexeme is not in the domain' do
      let(:new_lexeme) { 'photoalbum:PhotoAlbum' }

      context 'when the frequency is specified' do
        let(:new_frequency) { 73.0 }

        it 'adds lexeme with given frequency' do
          expect(lexicon.add_lexeme(domain, new_lexeme, new_frequency)).to eql(true)
          expect(lexicon.get_frequency(domain, new_lexeme)).to eql(new_frequency)
        end
      end

      context 'when the frequency is not specified' do
        let(:min_frequency) { 1.0 }

        it 'adds lexeme with default minimum frequency' do
          expect(lexicon.add_lexeme(domain, new_lexeme)).to eql(true)
          expect(lexicon.get_frequency(domain, new_lexeme)).to eql(min_frequency)
        end
      end
    end
  end

  describe '.domain_exists?' do
    context 'when domain is a string' do
      let(:new_domain) { 'history' }

      context 'when domain does not exist' do
        it 'returns false' do
          expect(lexicon.domain_exists?(new_domain)).to eql(false)
        end
      end

      context 'when domain exists' do
        it 'returns true' do
          expect(lexicon.domain_exists?(domain)).to eql(true)
        end
      end
    end

    context 'when domain is an Array' do
      context 'when none of the domains exist' do
        let(:new_domains) { ['history', 'philosophy'] }

        it 'returns false' do
          expect(lexicon.domain_exists?(new_domains)).to eql(false)
        end
      end

      context 'when any one of the domains exist' do
        let(:new_domains) { [domain, 'sports'] }

        it 'returns true' do
          expect(lexicon.domain_exists?(new_domains)).to eql(true)
        end
      end
    end

  end

  describe '.search' do
    context 'when none of the domains exist' do
      let(:params) { {domains: ['sports'], query: 'media' } }

      it 'returns an empty array' do
        expect(lexicon.search(params)).to eql([])
      end
    end

    context 'when some domains exist' do
      context 'when the query string is not in the domains' do
        let(:params) { {domains: domains, query: 'media' } }

        it 'returns empty array' do
          expect(lexicon.search(params)).to eql([])
        end
      end

      context 'when the query string is in the domains' do
        let(:params) { {domains: domains, query: 'photo' } }

        it 'returns an array of matches' do
          expect(lexicon.search(params)).to match_array(['Photoshop'])
        end

        context 'when the same query string is in more than one domain' do
          before do
            lexicon.add_lexeme('art', 'photoshop:Photoshop')
          end

          it 'returns a unique array of the matches' do
            expect(lexicon.search(params).count('Photoshop')).to eql(1)
          end
        end

        context 'when each member of the domains has different frequency' do
          before do
            lexicon.add_lexeme('photography', 'photostudio:PhotoStudio', 4)
            lexicon.add_lexeme('art', 'photobooth:PhotoBooth', 5)
          end

          it 'returns the matched values in descending order of their frequency' do
            expect(lexicon.search(params)).to eql(['Photoshop', 'PhotoBooth', 'PhotoStudio'])
          end
        end
      end
    end
  end
end

