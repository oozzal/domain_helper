# Generate code coverage whenever required with:
# COVERAGE=true bundle exec rspec
if ENV['COVERAGE']

  require 'simplecov'

  SimpleCov.start do
    add_filter '/spec/'
    add_filter '/config/'
    add_filter '/tmp/'
    add_filter '/log/'
  end
end

# setup test environment
ENV['RACK_ENV'] = 'test'

require 'rubygems'
require 'bundler'
Bundler.require(:default, :test)

require File.expand_path("../../app.rb", __FILE__)

RSpec.configure do |config|
  config.include Rack::Test::Methods
end

