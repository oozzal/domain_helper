require 'spec_helper'

describe DomainHelper::App do
  # (rspec)/(rack-test) expects app
  let(:app) { DomainHelper::App }
  let(:response) { JSON.parse(last_response.body).with_indifferent_access }
  let(:lexicon) { DomainHelper::Lexicon }
  let(:parser) { DomainHelper::QueryParser }

  describe 'GET /search' do
    let(:url) { '/search' }

    context 'when params are invalid' do
      context 'when params are not present' do
        it 'returns params required' do
          get url
          expect(response[:message]).to eql('params required')
          expect(response[:code]).to eql(400)
        end
      end

      context 'when query param is invalid' do
        before do
          allow(parser).to receive(:is_valid?).and_return(false)
        end

        it 'returns invalid query' do
          get url, {query: '%'}
          expect(response[:message]).to eql('invalid query')
          expect(response[:code]).to eql(400)
        end
      end

      context 'when domains do not exist' do
        before do
          allow(lexicon).to receive(:domain_exists?).and_return(false)
        end

        it 'returns invalid domain' do
          get url, {domains: ['dummy'], query: 'photo'}
          expect(response[:message]).to eql('invalid domains')
          expect(response[:code]).to eql(400)
        end
      end
    end

    context 'when params are valid' do
      context 'when query param is in the domain' do
        before do
          allow(lexicon).to receive(:domain_exists?).and_return(true)
          allow(lexicon).to receive(:search).and_return(['Photoshop', 'PhotoStudio'])
        end

        it 'returns an array of matches' do
          get url, {domains: ['photography'], query: 'photo'}
          expect(response[:results]).to include('Photoshop')
          expect(response[:code]).to eql(200)
        end
      end

      context 'when query param is not in the domain' do
        before do
          allow(lexicon).to receive(:domain_exists?).and_return(true)
          allow(lexicon).to receive(:search).and_return([])
        end

        it 'returns not found' do
          get url, {domains: ['photography'], query: 'photo'}
          expect(response[:message]).to eql('not found')
          expect(response[:code]).to eql(404)
        end
      end
    end
  end
end

