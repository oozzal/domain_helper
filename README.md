# Autocomplete helper powered by Redis
  * Uses Redis Sorted Set and the magical method: `zscan`

# Start the server
  * `RACK_ENV=<ENV> rackup -p <PORT>`
    + ENV:
      - `development`, `production`

# Search
  * `/search?domains[]=photography&query=photo`

# Testing
  * `bundle exec rspec`

  * Generate code coverage report
    + `COVERAGE=true bundle exec rspec`
    + (the report is saved to `/coverage` directory)

